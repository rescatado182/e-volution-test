<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $title_for_layout; ?></title>
		<?php
			echo $this->Html->meta('icon');

			echo $this->Html->css('bootstrap.min.css');
			echo $this->Html->css('bootstrap-responsive.min.css');

			echo $this->Html->script('jquery/jquery.min.js');

			// jQuery UI Datepicker
			echo $this->Html->script('jquery/jquery-ui.min.datepicker.js');
			echo $this->Html->css('jquery-ui.min.css');
			echo $this->Html->css('jquery.ui.theme.css');
			echo $this->Html->css('jquery.ui.theme.font-awesome.css');

			echo $this->Html->script('bootstrap.min.js');

			echo $this->Html->css('styles.css');

			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
	</head>
	<body>
		<div class='container'>
		    <h1>APLICACIÓN E-VOLUTION - GESTION DE TAREAS</h1>
		    <div class='navbar navbar-inverse'>
		      	<div class='navbar-inner nav-collapse' style="height: auto;">
			        <ul class="nav">
		          		<li class="active">
		          			<a href="#">Inicio</a>
	          			</li>
						<li>
							<?php echo $this->Html->link('Tareas', array('controller' => 'tasks', 'action' => 'index' )); ?>
						</li>
			        </ul>
	      		</div>
	    	</div>

	    	<div id='content' class='row-fluid'>
    			<div class='span2 sidebar'>
        			<h3>Left Sidebar</h3>
			        <ul class="nav nav-tabs nav-stacked">
			          <li><a href='#'>Another Link 1</a></li>
			          <li><a href='#'>Another Link 2</a></li>
			          <li><a href='#'>Another Link 3</a></li>
			        </ul>
		      	</div>

	      		<div class='span9 main'>
							<?php echo $this->Help->sessionFlash(); ?>
							<?php echo $this->fetch('content'); ?>
				</div>
			</div>
		</div>

	</body>
</html>
