<?php 
echo $this->Html->script(array('ckeditor/ckeditor.js'));
if (empty($modelClass)) {
    $modelClass = Inflector::singularize($this->name);
}
$this->Html
	->addCrumb('<i class="icon-home icon-large"></i>', '/admin', array('escape' => false))
	->addCrumb('Generales', array('action' => 'index'))
	->addCrumb($this->data['Section']['name'], array('action' => 'edit', $this->data['Section']['id']));
?> 
<h2 class="hidden-desktop">Editar Texto » <?php echo $this->data['Section']['name']; ?></h2>
<?php echo $this->Form->create($modelClass);?>

<div class="row-fluid">
	<div class="span8">
		<ul class="nav nav-tabs">
			<?php
			echo $this->Help->adminTab('Español', '#general-main-es');
			echo $this->Help->adminTab('Inglés', '#general-main-en');
			echo $this->Help->adminTabs();
			?>
		</ul>
		<div class="tab-content">
			<div id="general-main-es" class="tab-pane">
				<?php
					echo $this->Form->input('id');
					$this->Form->inputDefaults(array(
						'class' => 'span10',
						'label' => false,
					));
				?>
				<div class="input text">
					<label>Texto - Español</label>
					<?php
						echo $this->Form->textarea('text_es', array(
							'class' => 'ckeditor'
						));
					?>
				</div>
			</div>
			<div id="general-main-en" class="tab-pane">
				<div class="input text">
					<label>Texto - Inglés</label>
					<?php
						echo $this->Form->textarea('text_en', array(
							'class' => 'ckeditor'
						));
					?>
				</div>
			</div>
			<?php echo $this->Help->adminTabs(); ?>
		</div>
	</div>
	<div class="span4">
		<div class="row-fluid">
			<div class="span12">
				<div class="box">
					<div class="box-title">
						<i class="icon-list"></i>
						Acciones
					</div>
					<div class="box-content">
						<?php echo $this->Form->button('Aplicar', array('class' => 'btn', 'name' => 'apply')); ?>
						<?php echo $this->Form->button('Guardar', array('class' => 'btn btn-success')); ?>
						<?php echo $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>