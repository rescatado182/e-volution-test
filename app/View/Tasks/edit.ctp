<?php

if (empty($modelClass)) {
    $modelClass = Inflector::singularize($this->name);
}

?>

<script type="text/javascript">

jQuery( function() {
  $( "#TaskExpirationdate" ).datepicker({
    dateFormat        : 'yy-mm-dd',
    minDate           : -1,
    showOtherMonths   : true,
    selectOtherMonths : true
  });
} );

</script>


<h2 class="hidden-desktop">Perfíl</h2>

    <div class="well well-sm">
      <?php
      	echo $this->Form->create($modelClass);
        echo $this->Form->input('id', array('type' => 'hidden', 'readonly' => true));

      	$this->Form->inputDefaults(array(
  				//'class' => 'span10',
  				'label' => false,
  			));
     ?>

        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label for="name">Tarea</label>
                    <?php echo $this->Form->input('task', array('class' => 'form-control', 'required' => 'required', "placeholder" => "Tarea a realizar" )); ?>
                </div>
                <div class="control-group">
                    <?php $options = array('baja' => 'Baja', 'media' => 'Media', 'alta' => 'Alta'); ?>
                    <label for="name">Prioridad</label>
                    <?php echo $this->Form->input('priority', array('class' => 'form-control', 'options' => $options, 'empty' => 'Seleccione', 'required' => 'required', "placeholder" => "Prioridad" )); ?>
                </div>

                <div class="input-group control-group date">
                    <label for="name">Fecha de Culminación</label>
                    <?php echo $this->Form->input('expirationdate', array('class' => 'form-control', 'required' => 'required', "placeholder" => "Fecha en que termina la tarea" )); ?>
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>

            </div>

            <div class="clerfix"></div>


            <div class="span12 right">
                <?php echo $this->Form->button('Guardar', array('class' => 'btn btn-primary')); ?>
                <?php echo $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
            </div>
        </div>

        <?php echo $this->Form->end(); ?>

    </div>
