<?php 
echo $this->Html->script(array('ckeditor/ckeditor.js'));
if (empty($modelClass)) {
    $modelClass = Inflector::singularize($this->name);
}
$this->Html
	->addCrumb('<i class="icon-home icon-large"></i>', '/admin', array('escape' => false))
	->addCrumb('Aspirantes', array('action' => 'index'));
?> 
<h2 class="hidden-desktop">Nuevo Texto General</h2>
<?php echo $this->Form->create($modelClass);?>

<div class="container-fluid well span6">
	<div class="row-fluid">
        <div class="span2" >
		    <img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle">
        </div>
        
        <div class="span8">
            <h3><?php echo $row['Applicant']['name']; ?></h3>
            <h6>Documento: <?php echo $row['Applicant']['cid']; ?></h6>
            <h6>Edad: <?php echo $row['Applicant']['age']; ?></h6>
            <h6>Ciudad: <?php echo $row['Applicant']['city']; ?></h6>
            <h6>Teléfono: <?php echo $row['Applicant']['telephone']; ?></h6>
            <h6>Celular: <?php echo $row['Applicant']['cellphone']; ?></h6>            
            <h6>Email: <?php echo $row['Applicant']['email']; ?></h6>
            <h6>Profesión: <?php echo $row['Applicant']['profession']; ?></h6>
            <h6>Experiencia: <?php echo $row['Applicant']['experience']; ?></h6>
        </div>
        
        <div class="span2">
            <div class="btn-group">
                <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
                    Action 
                    <span class="icon-cog icon-white"></span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#"><span class="icon-wrench"></span> Modificar</a></li>
                    <li><a href="#"><span class="icon-trash"></span> Borrar</a></li>
                </ul>
            </div>
        </div>
</div>
</div>