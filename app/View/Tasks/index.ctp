<?php

  if (empty($modelClass)) {
    $modelClass = Inflector::singularize($this->name);
  }
?>

<script type="text/javascript">

  jQuery( function() {

    $(".close, .close-x").click( function() {
      $('#myModal').detach();
    });

  });

</script>


<div class="col-md-5  toppad  pull-right col-md-offset-3 ">
    <?php
        if( !empty($row['name']) )
            echo $this->Html->link('Editar Pérfil', array('controller' => 'users', 'action' => 'edit', $row['id'] ));
        else
            echo $this->Html->link('Crear Tareas - ', array('controller' => 'tasks', 'action' => 'add' ));
    ?>

    <?php echo $this->Html->link('Salir', array('controller' => 'users', 'action' => 'logout' )); ?>

    <br>

    <p class=" text-info"></p>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >


    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Tareas para <?php echo $user['username']; ?></h3>
        </div>

        <div class="panel-body">
          <div class="row-fluid">
              <div class="span12 actions">
                  <ul class="nav-buttons">
                      <li><?php echo $this->Html->link('Nuevo', array('action' => 'add'), array('class' => 'btn btn-success')); ?></li>
                  </ul>
              </div>
          </div>
          <div class="row-fluid">
              <div class="span12">
                  <table class="table table-striped">
                      <?php
                      $tableHeaders = array(
                          $this->Paginator->sort('id', 'ID'),
                          $this->Paginator->sort('task', 'Tareas'),
                          $this->Paginator->sort('priority', 'Prioridad'),
                          $this->Paginator->sort('expiration_date', 'Fecha de Culminación'),
                          $this->Paginator->sort('daystodaily', 'Días para terminar'),
                          'Acciones'
                      );
                      $tableHeaders = $this->Html->tableHeaders($tableHeaders);
                      ?>
                      <thead>
                          <?php echo $tableHeaders; ?>
                      </thead>
                      <?php

                        $flags = 0;
                        $rows = array();
                        foreach ($sections as $item) {

                          $priority = null;

                          // Priority
                          switch ($item[$modelClass]['priority'])
                          {
                            case 'alta':
                              $priority = '<span class="label label-important">Alta</span>';
                            break;

                            case 'media':
                            default:
                              $priority = '<span class="label label-warning">Media</span>';
                            break;

                            case 'baja':
                              $priority = '<span class="label label-info">Baja</span>';
                            break;
                          }

                          $daystodaily = null;


                          if ( $item[$modelClass]['daystodaily'] <= 3 ) {
                            $daystodaily = '<span class="label label-important">'.$item[$modelClass]['daystodaily'].'</span>';
                            $flags++;
                          }
                          elseif ( $item[$modelClass]['daystodaily'] >= 4 && $item[$modelClass]['daystodaily'] <= 8 ) {
                            $daystodaily = '<span class="label label-warning">'.$item[$modelClass]['daystodaily'].'</span>';
                          }
                          else {
                            $daystodaily = '<span class="label label-success">'.$item[$modelClass]['daystodaily'].'</span>';
                          }


                          $actions = '<div class="item-actions">' .
                              /*$this->Html->link('<i class="icon-eye-open icon-large"></i>',
                                  array('action' => 'view', $item[$modelClass]['id']),
                                  array('data-title' => 'Ver este elemento', 'rel' => 'tooltip', 'data-trigger' => 'hover', 'escape' => false, 'class' => 'edit')
                              ) .*/
                              '&nbsp;&nbsp;'
                              .
                              $this->Html->link('<i class="icon-edit icon-large"></i>',
                                  array('action' => 'edit', $item[$modelClass]['id']),
                                  array('data-title' => 'Editar este elemento', 'rel' => 'tooltip', 'data-trigger' => 'hover', 'escape' => false, 'class' => 'edit')
                              ) .
                              '&nbsp;&nbsp;'
                              .
                              $this->Html->link('<i class="icon-remove icon-large"></i>',
                                  array('action' => 'delete', $item[$modelClass]['id']),
                                  array('data-title' => 'Eliminar este elemento', 'rel' => 'tooltip', 'data-trigger' => 'hover', 'escape' => false, 'class' => 'edit')
                              ) .
                          '</div>';
                          $rows[] = array(
                              $item[$modelClass]['id'],
                              $item[$modelClass]['task'],
                              $priority,
                              $item[$modelClass]['expiration_date'],
                              $daystodaily,
                              $actions
                          );
                        }
                      ?>
                      <?php echo $this->Html->tableCells($rows); ?>
                  </table>

              </div>
          </div>

          <div class="row-fluid">
              <div class="span12">
                  <?php if ($pagingBlock = $this->fetch('paging')): ?>
                      <?php echo $pagingBlock; ?>
                  <?php else: ?>
                      <?php if (isset($this->Paginator) && isset($this->request['paging'])): ?>
                          <div class="pagination">
                              <p>
                              <?php
                              echo $this->Paginator->counter(array(
                                  'format' => 'Página {:page} de {:pages}, mostrando {:current} registros de {:count} totales, iniciando en el registro {:start}, finalizando en {:end}'
                              ));
                              ?>
                              </p>
                              <ul>
                                  <?php echo $this->Paginator->first('< ' . 'first'); ?>
                                  <?php echo $this->Paginator->prev('< ' . 'prev'); ?>
                                  <?php echo $this->Paginator->numbers(); ?>
                                  <?php echo $this->Paginator->next('next' . ' >'); ?>
                                  <?php echo $this->Paginator->last('last' . ' >'); ?>
                              </ul>
                          </div>
                      <?php endif; ?>
                  <?php endif; ?>
              </div>
          </div>

    </div>
</div>


<!-- Modal -->
<div id="myModal" class="<?php echo ($flags > 0 ? 'modal' : 'fade'); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Gestión de Tareas</h3>
  </div>
  <div class="modal-body">
    <h4>Hola <?php echo $user['username']; ?></h4>
    <p>Este es un recordatorio de tus tareas pendientes</p>

    <p>Tienes <strong><?php echo $flags ?></strong> pendientes, próximas a vencer, revisa el listado a continuación para más detalles</p>

  </div>
  <div class="modal-footer">
    <button class="btn btn-primary close-x" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
