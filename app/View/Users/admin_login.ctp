<?php echo $this->Form->create('User'); ?>
<div class="box">
	<div class="box-content">
	<?php
		$this->Form->inputDefaults(array(
			'label' => false,
		));
		echo $this->Form->input('username', array(
			'placeholder' => 'Usuario',
			'before' => '<span class="add-on"><i class="icon-user"></i></span>',
			'div' => 'input-prepend text',
			'class' => 'span11',
		));
		echo $this->Form->input('password', array(
			'placeholder' => 'Contraseña',
			'before' => '<span class="add-on"><i class="icon-key"></i></span>',
			'div' => 'input-prepend password',
			'class' => 'span11',
		));
		echo $this->Form->button('Ingresar', array('class' => 'btn'));
	?>
	</div>
</div>
<?php echo $this->Form->end(); ?>