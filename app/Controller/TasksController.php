<?php

class TasksController extends AppController {

    public $name        = 'Tasks';
    public $helpers     = array('Html', 'Form', 'Session');
    public $components  = array('Session', 'RequestHandler', 'Auth', 'Acl');


    public function index()
    {
        $user = $this->Auth->user();

        if (!empty($user['username']) ) {
            $this->set('row', $this->Task->findByUser_id($user['id']) );
            $this->set('user', $user);
        } else {
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->Task->recursive = 0;
        $this->set( 'sections', $this->paginate( array('Task.user_id' => $user['id'])) );
    }

    public function add()
    {

        if (!empty($this->request->data))
        {
            $user = $this->Auth->user();
            $this->Task->create();

            $this->request->data['Task']['creation_date']   = date("Y-m-d");
            $this->request->data['Task']['expiration_date'] = $this->request->data['Task']['expirationdate'];
            $this->request->data['Task']['user_id']         = $user['id'];

            if ($this->Task->save($this->request->data)) {
                $this->Session->setFlash('El registro se ha creado exitosamente.', 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Error al intentar crear el registro. Por favor, intente nuevamente.', 'default', array('class' => 'error'));
            }
        }

        $this->render('edit');
    }

    public function edit($id = null)
    {
        if (!empty($this->request->data))
        {
            $user = $this->Auth->user();

            $this->request->data['Task']['expiration_date'] = $this->request->data['Task']['expirationdate'];
            $this->request->data['Task']['user_id']         = $user['id'];

            if ($this->Task->save($this->request->data))
            {
                $this->Session->setFlash('Actualización exitosa', 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));

            } else {
                $this->Session->setFlash('Error en la actualización', 'default', array('class' => 'error'));
            }
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->Task->read(null, $id);
        }
    }

    public function delete($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(__('ID no válido'), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Task->delete($id)) {
            $this->Session->setFlash(__('El registro ha sido borrado exitosamente.'), 'default', array('class' => 'success'));
            $this->redirect(array('action' => 'index'));
        }
    }


    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }
        parent::beforefilter();
    }

    public function admin_index() {
        $this->layout = 'admin';
        $this->Task->recursive = 0;
        $this->set('sections', $this->paginate());
    }


    public function admin_view($id = null)
    {
        $this->layout = 'admin';
        if ($id)
            $this->set('row', $this->Task->read(null, $id) );

    }

}

?>
