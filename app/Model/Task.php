<?php

class Task extends AppModel {

	public $name = 'Task';


	public $virtualFields = array(
    'daystodaily' => 'DATEDIFF( expiration_date, NOW() )'
	);

}
